import requests
import json
import os, sys
import glob
import pandas as pd
import math

#####################RECUPERER METADATA des DONNEES DANS COLLECTION NAKALA#####################################################################

#var_api_key = MY NAKALA API KEY

#FICHIER RECUPERATION
header = "bloc,image,locus,campagne,date,heure,appareil,idnakala,sha1,iiif"
f = open("data_1.txt", "a")
f.write(header + "\n")
f.close()

var_url_iiif = 'https://api.nakala.fr/iiif/'
var_json = '/info.json'

#FICHIER LISTE ID COLLECTION NAKALA _ colonne Locus et colonne ID collection Nakala
df1 = pd.read_excel("locus.xlsx")
long = len(df1)

#DANS CHAQUE COLLECTION NAKALA
for k in range(0,long):
   var_col = (df1["Locus"][k])
   var_nakcol = (df1["Collection"][k])
   url = 'https://api.nakala.fr/collections/'+var_nakcol+'/datas/?page=1&limit=25'
   verb = "GET"
   apiKey = var_api_key
   headers = {
     'X-API-KEY': apiKey
   }
   #ON RECUPERE LE NB DE DATA ET ON CALCUL NB PAGES
   response = requests.request(verb, url, headers=headers)
   parsed = json.loads(response.text)
   print(parsed['total'])
   var_total = parsed['total']
   var_page = var_total/25
   page_numb = math.ceil(var_page)
   pnumb=int(page_numb)

   #POUR CHAQUE PAGE NAKALA QUI CONTIENT 25 ENTREES
   for i in range(1, pnumb + 1):
      url2 = 'https://api.nakala.fr/collections/'+var_nakcol+'/datas/?page='+str(i)+'&limit=25'
      verb = "GET"
      apiKey = var_api_key
      headers = {
        'X-API-KEY': apiKey
      }
     
      response2 = requests.request(verb, url2, headers=headers)
      parsed_result = json.loads(response2.text)
      pdata = parsed_result['data']
      #ON RECUPERE LES METADATA DE LA DONNEE ET ON ECRIT LA LIGNE DANS LE FICHIER
      for j in range(0, 25):
         try:
            var_data = pdata[j]
            var_files = var_data['files']
            var_sha = var_files[0]['sha1']
            var_id = var_data['identifier']
            var_meta = var_data['metas']
            var_name_image = var_meta[0]['value']
            var_name = var_name_image[0:-2]
            var_date = var_meta[1]['value']
            var_heure = var_meta[4]['value']
            var_appareil = var_meta[5]['value']
            var_campagne = var_meta[6]['value']
            var_iiif = var_url_iiif + var_id + '/' + var_sha + var_json
            #print(var_iiif)
            #print(var_files)
            #print(var_sha)
            #print(var_id)
            #print(var_name)
            #print(var_heure)
            #print(var_date)
            #print(var_appareil)
            #print(var_campagne)
            f = open("data_1.txt", "a")
            f.write(var_name_image + "," + var_name + "," + var_col + "," + var_campagne + "," + var_date + "," + var_heure + "," + var_appareil + "," + var_id + "," + var_sha + "," + var_iiif +"\n")
            f.close()
         except IndexError:
            pass
         continue
      #print(json.dumps(json.loads(response2.text), indent=4))
      print('_________________________________________________________________________________________________________________')

