import requests
import json
import os, sys
import glob
import pandas as pd
import math

##########################CREER COLLECTION NAKALA##################################################################

#RENSEIGNER API KEY
#var_key_nakala = MY API KEY NAKALA

header = "locus,collection"
fi = open("collection_1.txt", "a")
fi.write(header + "\n")
fi.close()

df1 = pd.read_excel("U:/Images WASRAP/locus_complement.xlsx")
long = len(df1)

for k in range(0,long):
   var_loc = (df1["Locus"][k])
   var_colname = 'WLocus ' + str(var_loc)
   print(var_colname)
   url = 'https://api.nakala.fr/collections'
   verb = 'POST'
   apiKey = var_key_nakala
   headers = {
       'X-API-KEY': apiKey,
       'Content-Type': 'application/json'
   }
 
   content = {
       "status": "public",
       "metas" : [
           {
               "propertyUri": "http://nakala.fr/terms#title",
               "value": var_colname,
               "typeUri": "http://www.w3.org/2001/XMLSchema#string",
               "lang": "fr"
           }
       ]
   }

   createCollectionResponse = requests.request(verb, url, headers=headers, data=json.dumps(content))
   r1 = json.loads(createCollectionResponse.text)
   r2 = r1['payload']
   id_nakala = r2['id']
   f = open("collection_1.txt", "a")
   f.write(var_colname + "," + id_nakala + "\n")
   f.close()
   print(r2)
   print(id_nakala)