import os
import os.path
import csv
import requests
import json
import glob
import pandas as pd
import warnings
warnings.simplefilter(action='ignore', category=FutureWarning)

##########################CREER COLLECTION NAKALA##################################################################

#RENSEIGNER API KEY
#var_key_nakala = MY API KEY NAKALA

#JOINTURE
df1 = pd.read_csv("U:/Images WASRAP/collection.csv", sep=";")
df2 = pd.read_csv("U:/Images WASRAP/image.csv")
tab = pd.merge(df1, df2, on = "Locus")
#print(tab)
#tab.to_csv('jointure.csv')
long = len(tab)
print(long)

#FICHIER DE RECUPERATION INFO METADATA
headerfile = "titre,image,locus,collection,campagne,date,heure,appareil,idnakala,file_nakala,sha_nakala"
fresult = open("metadata.txt", "a")
fresult.write(headerfile + "\n")
fresult.close()

url = "https://api.nakala.fr/datas/uploads"
verb = "POST"
apiKey = var_key_nakala
headers = {
  'X-API-KEY': apiKey
}

a = 0
for i in range(0, long):
   a = a + 1
   fileimage = tab["image"][i]
   #print(fileimage)
   var_locus = str(tab["Locus"][i])
   #print(var_locus)
   imagepath = ("U:/Images WASRAP/w_6/" + var_locus + "/" + fileimage)
   #print(imagepath)
   loc_collection = tab["collection"][i]
   #print(loc_collection)
   titre_data = tab["titre"][i]
   #print(titre_data)
   var_date = str(tab["date"][i])
   #print(var_date)
   var_heure = str(tab["heure"][i])
   #print(var_heure)
   var_medium = tab["appareil"][i]
   #print(var_heure)
   var_medium = tab["appareil"][i]
   var_campagne = tab["campagne"][i]
   #print(var_campagne)
   content = {}
   files=[('file',(fileimage, open(imagepath, 'rb')))]
   responseUpload = requests.request(verb, url, headers=headers, data=content, files=files)
   # Affichons le contenu de la réponse de l'API
   if responseUpload.status_code == 201: 
      fileMetadata = responseUpload.text
      #print(fileMetadata)
   else:
      print(responseUpload.text)

   fileMetadataJson = json.loads(fileMetadata)
   
   url2 = 'https://api.nakala.fr/datas'
   verb2 = 'POST'
   headers2 = {'X-API-KEY': apiKey,'Content-Type': 'application/json'}
   content = {
      "status": "published",
      "files" : [
       # On ajoute ici les informations sur le fichier uploadé précédemment
       fileMetadataJson
       ],
       # On ajoute ici quelques métadonnées pour décrire la donnée
       "metas" : [
          # le titre de la donnée
          {
             "propertyUri": "http://nakala.fr/terms#title",
             "value": titre_data,
             "typeUri": "http://www.w3.org/2001/XMLSchema#string",
             "lang": "fr"
           },
           # le type de la donnée (image)
           {
              "propertyUri": "http://nakala.fr/terms#type",
              "value": "http://purl.org/coar/resource_type/c_c513",
              "typeUri": "http://www.w3.org/2001/XMLSchema#anyURI",
           },
           # le créateur de la donnée
          {
              "propertyUri": "http://nakala.fr/terms#creator",
              "value": {
                 "givenname": "Gwenola",
                 "surname": "Graff"
              }
           },
           # la date de création de la donnée
           {
              "propertyUri": "http://nakala.fr/terms#created",
              "value": var_date,
              "typeUri": "http://www.w3.org/2001/XMLSchema#string"
           },
           {
              "propertyUri": "http://purl.org/dc/terms/issued",
              "value": var_heure,
              "typeUri": "http://www.w3.org/2001/XMLSchema#string"
           }
             ,
           # la licence associée à la donnée
           {
              "propertyUri": "http://nakala.fr/terms#license",
              "value": "CC-BY-4.0",
              "typeUri": "http://www.w3.org/2001/XMLSchema#string"
           },
           #MEDIUM appareil photo
           {
              "propertyUri": "http://purl.org/dc/terms/medium",
              "value": var_medium,
              "typeUri": "http://www.w3.org/2001/XMLSchema#string"
           },
           
           #CAMPAGNE
           {
              "propertyUri": "http://purl.org/dc/terms/temporal",
              "value": "WASRAP 9",
              "typeUri": "http://www.w3.org/2001/XMLSchema#string"
           },
   
       ],
	
      "collectionsIds": [loc_collection]
   }

   createDataResponse = requests.request(verb, url2, headers=headers2, data=json.dumps(content))
   r1 = json.loads(createDataResponse.text)
   r2 = r1['payload']
   #print(r1)
   #print(r2)
   id_nakala = r2['id']
   file_name = fileMetadataJson['name']
   sha1_name = fileMetadataJson['sha1']
   fresult = open("metadata.txt", "a")
   fresult.write(titre_data + "," + fileimage + "," + var_locus + "," + loc_collection + "," + "WASRAP 10" + "," + var_date + "," + var_heure + "," + var_medium + "," + id_nakala + "," + file_name + "," + sha1_name + "\n")
   fresult.close()


else:
   print('probleme pour ' + fileimage)
print(a)